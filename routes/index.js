var express = require('express');
var router = express.Router();

var userController = require('../controllers/user');

/* GET home page. */
router.get('/', function(req, res, next) {
    res.render('seabattle/index', { title: 'Seabattle' });
});

/* GET admin page. */
router.get('/admin/', function(req, res, next) {
    res.render('index', { title: 'Admin', layout: 'admin_layout' });
});

/* GET seabattle page. */
router.get('/seabattle/', function(req, res, next) {
    res.render('seabattle/index', { title: 'Seabattle' });
});


module.exports = router;