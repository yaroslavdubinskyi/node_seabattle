var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var exphbs  = require('express-handlebars');
var sassMiddleware = require('node-sass-middleware');
var session = require('express-session');
var MongoStore = require('connect-mongo')(session);
var passport = require('passport');
var mongoose = require('mongoose');
var randomID = require("random-id");

var secrets = require('./config/secrets');
var index = require('./routes/index');

mongoose.connect(secrets.db, {
    useMongoClient: true
});
require('./config/passport')(passport);

var app = express();

// call socket.io to the app
app.io = require('socket.io')();

// view engine setup
app.engine('.hbs', exphbs({
    layoutsDir: __dirname + '/views/layouts',
    defaultLayout: 'layout',
    extname: '.hbs'
}));
app.set('view engine', '.hbs');


// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(sassMiddleware({
  src: path.join(__dirname, 'public'),
  dest: path.join(__dirname, 'public'),
  indentedSyntax: false, // true = .sass and false = .scss
  sourceMap: true
}));
app.use(express.static(path.join(__dirname, 'public')));


//Express sessions
var sessionMiddleware = session({
    resave: true,
    saveUninitialized: true,
    secret: secrets.sessionSecret,
    store: new MongoStore({
        mongooseConnection: mongoose.connection
    }) //url: secrets.db, auto_reconnect: true
});

app.io.use(function(socket, next) {
    sessionMiddleware(socket.request, socket.request.res, next);
});

app.use(sessionMiddleware);

//Passport init
app.use(passport.initialize());
app.use(passport.session());

// routes
app.use('/', index);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});


var usernames = {};
var rooms = {};
app.io.on('connection', function(socket){
    if(Object.keys(rooms).length !== 0){
        app.io.sockets.emit("update-rooms-list", rooms);
    }
    socket.on("player_join_new_rooom", function(username, user_ships, opponent_ships){
        var room_id = randomID(10);
        socket.username = username;
        socket.room = room_id;
        socket.ships = user_ships;

        usernames[username] = {
            name: username,
            room: room_id,
            ships: user_ships,
            opponent_ships: opponent_ships
        };

        rooms[room_id] = {
            id: room_id,
            name: 'Room_' + room_id,
            player_1: username
        };

        socket.join(room_id);
        socket.emit("update_user_gamefield", user_ships);
        socket.emit("update_user_name_info", username);
        app.io.sockets.in( room_id ).emit("update_user_room_info", rooms[room_id]);

        socket.broadcast.to(room_id).emit('updatechat', 'SERVER', username + ' has connected to this room');

        app.io.sockets.emit("update-players", usernames);
        app.io.sockets.emit("update-rooms-list", rooms);



    });
    socket.on("player_join_choosen_rooom", function(username, user_ships, opponent_ships, room_id){
        socket.username = username;
        socket.room = room_id;
        socket.ships = user_ships;

        usernames[username] = {
            name: username,
            room: room_id,
            ships: user_ships,
            opponent_ships: opponent_ships
        };

        rooms[room_id].player_2 = username;
        rooms[room_id].turn = rooms[room_id].player_1;

        socket.join(room_id);
        socket.emit("update_user_gamefield", user_ships);
        socket.emit("update_user_name_info", username);
        app.io.sockets.in( room_id ).emit("update_user_room_info", rooms[room_id]);

        socket.broadcast.to(room_id).emit('updatechat', 'SERVER', username + ' has connected to this room');

        app.io.sockets.in( room_id ).emit('updatechat', 'SERVER', 'Game started!');
        app.io.sockets.in( room_id ).emit('game_started', rooms[room_id].turn);
        app.io.sockets.emit("update-players", usernames);
        app.io.sockets.emit("update-rooms-list", rooms);
    });

    socket.on("click_opponent_cell", function (row, col) {
        var user_ships;
        var opponent_ships;
        var opponent_name;
        var current_socket_name = socket.username;
        if(current_socket_name === rooms[socket.room].player_1){
            opponent_name = rooms[socket.room].player_2;
            user_ships = usernames[opponent_name].ships;
            opponent_ships = usernames[opponent_name].opponent_ships;
            if(user_ships[row-1][col-1] === 1){
                user_ships[row-1][col-1] = 3;
                opponent_ships[row-1][col-1] = 3;
            }else{
                user_ships[row-1][col-1] = 2;
                opponent_ships[row-1][col-1] = 2;
            }
            usernames[opponent_name].ships = user_ships;
            usernames[opponent_name].opponent_ships = opponent_ships;
        }else{
            opponent_name = rooms[socket.room].player_1;
            user_ships = usernames[opponent_name].ships;
            opponent_ships = usernames[opponent_name].opponent_ships;
            if(user_ships[row-1][col-1] === 1){
                user_ships[row-1][col-1] = 3;
                opponent_ships[row-1][col-1] = 3;
            }else{
                user_ships[row-1][col-1] = 2;
                opponent_ships[row-1][col-1] = 2;
            }
            usernames[opponent_name].ships = user_ships;
            usernames[opponent_name].opponent_ships = opponent_ships;
        }

        socket.broadcast.to( socket.room ).emit('update_user_field', user_ships);
        app.io.sockets.in( socket.room ).emit('update_opponent_field', opponent_ships, current_socket_name);
        app.io.sockets.in( socket.room ).emit('next_turn', current_socket_name);
    });


    socket.on("disconnect", function(){
        socket.broadcast.to(socket.room).emit('updatechat', 'SERVER', socket.username + ' leaved this room');
        delete usernames[socket.username];

        if(Object.keys(rooms).length !== 0 && socket.room !== undefined){
            if((rooms[socket.room].player_2 === socket.username)||(rooms[socket.room].player_1 === socket.username)){
                delete rooms[socket.room];
            }
        }

        app.io.sockets.in( socket.room ).emit("update_user_room_info", rooms[socket.room]);
        app.io.sockets.emit("update-players", usernames);
        app.io.sockets.emit("update-rooms-list", rooms);
    });
});


module.exports = app;
