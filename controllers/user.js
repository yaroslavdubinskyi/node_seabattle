var passport = require('passport');
var mongoose = require('mongoose');
var User = require('../models/User');
var config = require('../config/secrets');

exports.isAdmin = function (req, res, next){
    if(req.isAuthenticated() && req.user.root === true ){
        next();
    }else{
        res.redirect("/user/login");
    }
};