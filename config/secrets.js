env = process.env.NODE_ENV || 'dev';

var config = {
    production: {
        port: process.env.PORT || 3000,
        db: 'mongodb://localhost:27017/node_chat',
        sessionSecret: 'super_node_secret'
    },
    dev: {
        port: process.env.PORT || 3000,
        db: 'mongodb://localhost:27017/node_chat',
        sessionSecret: 'super_node_secret'
    }
};


module.exports = config[env];